# Shared Open Frontend Web Sandbox
A shared sandbox project for HTML+CSS things you would like to try, test or share.

**Each collaborator has their own papge. Please see below how to have your own page.**

## Setup
- Install **Node.js** and **git**.
- Run the command on your CMD or terminal: `git clone https://gitlab.com/esfox/frontend-sandbox.git`.
- Run the command: `cd frontend-sandbox`
- Run the following command (replace `(page_name)` with your own custom name):  
`node add (page_name)`.
- To see your page, open it (the `.html` file) on a browser.
- To edit your page, find it in `/public/sandboxes` and open it in your text editor. (Note: You need to refresh the page on the browser if you want to see the changes you make.)

## Saving your page
- Save your page (if you haven't yet).
- In your CMD or terminal, run the command: `git add public/sandboxes`
- Run the command: `git commit -m "Updated a page"`
- Run the command: `git push`
