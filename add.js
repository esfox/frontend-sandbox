const fs = require('fs');

const name = process.argv[2];
if(!name)
  return console.log('Please enter a name for the page.');

/** @type {[]} */
const list = require('./public/sandboxes/list.json');

if(list.includes(name))
  return console.log("There's already a page with that name.");

list.push(name);

fs.writeFileSync(`./public/sandboxes/${name}.html`, '<div>Hello World!</div>');
fs.writeFileSync('./public/sandboxes/list.json', JSON.stringify(list));

console.log(`Created page ${name}.html in /public/sandboxes.`);
