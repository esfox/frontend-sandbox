import React, { useState, useEffect } from 'react';

import './styles/App.css';

export function App()
{
  const [ sandboxes, setSandboxes ] = useState([]);
  const [ page, setPage ] = useState();
  const [ isLoading, setIsLoading ] = useState();
  
  window.history.pushState(null, null, window.location.href);
    
  /** @param {Event} event */
  window.onpopstate = event =>
  {
    event.preventDefault();
    setPage();
  };

  useEffect(() =>
  {
    const loadSandboxes = async () =>
    {
      const loadedSandboxes = await fetch('/sandboxes/list.json')
        .then(response => response.json())
        .catch(console.error);

      setSandboxes(loadedSandboxes);
    }

    loadSandboxes();
  }, []);

  const loadPage = async sandbox =>
  {
    setIsLoading(true);
    const html = await fetch(`/sandboxes/${sandbox}.html`)
      .then(response =>
      {
        console.log(response);
        return response;
      })
      .then(response => response.text())
      .catch(console.error)
    setPage(html);
    setIsLoading(false);
  };

  return (
    page? <div dangerouslySetInnerHTML={{ __html: page }}/> :
    <div id="landing">

      <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossOrigin="anonymous"
      ></link>
      <h1 className="text-center mb-5">Sandboxes</h1>
      <div id="devs" className="row justify-content-center">
      {
        sandboxes.map((sandbox, i) =>
          <div key={i} className="card p-4 mx-3 mb-3" onClick={() => loadPage(sandbox)}>
            <h2>{sandbox}</h2>
          </div>
        )
      }
      </div>

      <div id="loading" className={isLoading? 'visible' : 'invisible'}>
        {[ 0, 1, 2 ].map(() => <span className="loading-element"/>)}
      </div>

    </div>
  );
}
